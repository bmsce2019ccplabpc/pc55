#include<stdio.h>
#include<math.h>
float input()
{
  float a;
  printf("enter the number");
  scanf("%f",&a);
  return 0;
}
float compute_distance(float x1,float x2,float y1,float y2)
{
  float distance;
  distance=sqrt((x2-x1)*(x2-x1)-(y2-y1)*(y2-y1));
  return distance;
}
void output (float x1,float x2,float y1,float y2,float distance)
{
  printf("distance between two points %f,%f and %f,%f is %4f",x1,x2,y1,y2,distance);
}
int main()
{
  float x1,x2,y1,y2,result;
  x1=input();
  y1=input();
  x2=input();
  y2=input();
  result=compute_distance(x1,x2,y1,y2);
  output(x1,x2,y1,y2,result);
  return 0;
}